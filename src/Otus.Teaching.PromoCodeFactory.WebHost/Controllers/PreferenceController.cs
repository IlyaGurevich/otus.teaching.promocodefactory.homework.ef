﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private IPreferenceRepository _repository; 

       public PreferenceController(IPreferenceRepository repository) 
        { 
            _repository= repository;
        }

        /// <summary>
        /// Получить все предпочтения пользователей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync() 
        {
            try
            {
                var preferences = await _repository.GetAllAsync();
                var response = (preferences as List<Preference>).Select(x => new PreferenceResponse
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
                return Ok(response);
            }
            catch (ArgumentException) { return BadRequest(); }
        }

        /// <summary>
        /// Получить предпочтение по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<PreferenceResponse>> GetPreferenceByIdAsync(Guid id)
        {
            try
            {
                var preference = await _repository.GetByIdAsync(id);
                var response = new PreferenceResponse
                {
                    Id = preference.Id,
                    Name = preference.Name
                };
                return Ok(response);
            }
            catch (ArgumentException) { return BadRequest(); }
        }
    }
}
