﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private ICustomerRepository _customerRepository;

        private IPreferenceRepository _preferenceRepository;



        public CustomersController(ICustomerRepository customerRepository, IPreferenceRepository  preferenceRepository) 
        {
            _customerRepository = customerRepository;
            _preferenceRepository= preferenceRepository;
        }
        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            //TODO: Добавить получение списка клиентов
            try
            {
                var customers = await _customerRepository.GetAllAsync();

                List<CustomerShortResponse> response = (customers as List<Customer>).Select(x =>
                new CustomerShortResponse
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email
                }).ToList();
                return Ok(response);
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        /// <summary>
        /// Получить клиента по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            try
            {

                var customer = await _customerRepository.GetByIdAsync(id);
               

                CustomerResponse responce = new CustomerResponse
                {
                    Id = customer.Id,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Email = customer.Email,
                    PromoCodes = customer.PromoCodes.Select(y => new PromoCodeShortResponse
                    {
                        Id = y.Id,
                        Code = y.Code,
                        ServiceInfo = y.ServiceInfo,
                        BeginDate = y.BeginDate.ToString(),
                        EndDate = y.EndDate.ToString(),
                        PartnerName = y.PartnerName
                    }).ToList(),
                    Preferences = customer.Preferences.Select(y => new PreferenceResponse
                    {
                        Id = y.Id,
                        Name = y.Name,
                    }).ToList()
                };
                return Ok(responce);
            }
            catch (ArgumentException)
            {
                return BadRequest();
    }
}
        /// <summary>
        /// Добавить нового клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями

            try
            {
                IEnumerable<Preference> preferences = await _preferenceRepository.GetByIdsAsync(request.PreferenceIds);

                Customer customer = new Customer
                {
                    Id = Guid.NewGuid(),
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Email = request.Email
                };

                customer.Preferences.AddRange(preferences);

                await _customerRepository.AddAsync(customer);

                return Ok();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        /// <summary>
        /// Изменить параметры клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>

        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            try
            {
                IEnumerable<Preference> preferences = await _preferenceRepository.GetByIdsAsync(request.PreferenceIds);

                Customer customer = new Customer
                {
                    Id = id,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Email = request.Email
                };

                customer.Preferences.AddRange(preferences);

                await _customerRepository.EditCustomer(id, customer);

                return Ok();
            }
            catch(ArgumentException) 
            {
                return BadRequest();
            }
        }
        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами

            try
            {
                if (await _customerRepository.GetByIdAsync(id) == null)
                    return BadRequest("Нет клиента с указанным Id");

                await _customerRepository.DeleteByIdAsync(id);

                return Ok();
            }
            catch(ArgumentException) 
            {
                return BadRequest();
            }
        }
    }
}