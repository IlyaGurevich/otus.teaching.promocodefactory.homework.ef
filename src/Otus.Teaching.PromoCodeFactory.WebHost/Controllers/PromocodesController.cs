﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {

        private IPromoCodeRepository _repositoryPromoCode;

        private IEmployeeRepository _repositoryEmploee;

        private ICustomerRepository _repositoryCustomer;

        private IPreferenceRepository _repositoryPreference;

        private DatabaseContext _databaseContext;

        public PromocodesController( DatabaseContext databaseContext,
            IPromoCodeRepository repositoryPromoCode, 
            IEmployeeRepository repositoryEmployee,
            ICustomerRepository repositoryCustomer,
            IPreferenceRepository repositoryPreference
            ) 
        { 
            _repositoryPromoCode= repositoryPromoCode;
            _repositoryEmploee= repositoryEmployee;
            _repositoryCustomer= repositoryCustomer;
            _repositoryPreference= repositoryPreference;
            _databaseContext= databaseContext;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            //TODO: Получить все промокоды 
            try
            {

                var promocodes = await _repositoryPromoCode.GetAllAsync();

                var response = promocodes.Select(x => new PromoCodeShortResponse
                {
                    Id = x.Id,
                    Code = x.Code,
                    BeginDate = x.BeginDate.ToString(),
                    EndDate = x.EndDate.ToString(),
                    PartnerName = x.PartnerName,
                    ServiceInfo = x.ServiceInfo
                }).ToList();
                return Ok(response);
            }
            catch ( ArgumentException) { return BadRequest(); }
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением

            try
            {

                Employee employee = await _repositoryEmploee.GetEmployeeByName(request.PartnerName);

                Preference preference = await _repositoryPreference.GetPreferenceByName(request.Preference);

                IEnumerable<Customer> customers = await _repositoryCustomer.GetCustomersByPreference(request.Preference);

                PromoCode promoCode = new PromoCode
                {
                    Id = Guid.NewGuid(),
                    Code = request.PromoCode,
                    ServiceInfo = request.ServiceInfo,
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(20),
                    PartnerName = request.PartnerName,
                    PartnerManager = employee,
                    PreferenceId = preference.Id,
                };

                foreach (var customer in customers)
                {
                    customer.PromoCodes.Add(promoCode);
                }

                employee.AppliedPromocodesCount += customers.Count();

                await _repositoryPromoCode.AddAsync(promoCode);

                _databaseContext.SaveChanges();

                return Ok();
            }
            catch(ArgumentException) { return BadRequest(); }

        }
    }
}