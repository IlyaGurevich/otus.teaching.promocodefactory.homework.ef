﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public static class ExtentionServices
    {
        /// <summary>
        /// Добавление контекста, добавление данных из статического класса, добавление данных в базу.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection ConfigureDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            string connectionString = configuration["ConnectionString"];
            services.AddDbContext<DatabaseContext>(optionBuilder =>
                optionBuilder
                .UseSqlite(connectionString));
            services.AddRepositories();
            services.InitializeDatabase();

            return services;

        }

        public static IServiceCollection InitializeDatabase(this IServiceCollection service)
        {
            var provider = service.BuildServiceProvider();
            DatabaseContext dbContext = provider.GetService<DatabaseContext>();
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();

            // Создание переменных для добавления данных в БД.
            List<Employee> employees = FakeDataFactory.Employees.ToList();
            List<Role> roles = FakeDataFactory.Roles.ToList(); 
            List<Preference> preferences = FakeDataFactory.Preferences.ToList(); 
            List<Customer> customers = FakeDataFactory.Customers.ToList();
            List<PromoCode> promoCodes = FakeDataFactory.PromoCodes.ToList();

            // Запись данных в БД.
            customers[0].Preferences.AddRange(preferences.Take(3));
            customers[1].Preferences.AddRange(preferences.Skip(1).Take(3));

            customers[0].PromoCodes.Add(promoCodes[0]);
            customers[1].PromoCodes.Add(promoCodes[1]);

            promoCodes[0].PartnerManagerId = employees[0].Id;
            promoCodes[0].PartnerName = employees[0].FullName;
            promoCodes[0].PreferenceId = preferences[0].Id;

            promoCodes[1].PartnerManagerId = employees[0].Id;
            promoCodes[1].PartnerName = employees[0].FullName;
            promoCodes[1].PreferenceId = preferences[3].Id;

            dbContext.Roles.AddRangeAsync(roles);
            dbContext.Employees.AddRangeAsync(employees);
            dbContext.Preferences.AddRangeAsync(preferences);
            dbContext.Customers.AddRangeAsync(customers);
            dbContext.PromoCodes.AddRangeAsync(promoCodes);

            dbContext.SaveChanges();
            return service;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services
                .AddScoped<IEmployeeRepository, EmployeeRepository>()
                .AddScoped<IRoleRepository, RoleRepository>()
                .AddScoped<IPreferenceRepository, PreferenceRepository>()
                .AddScoped<ICustomerRepository, CustomerRepository>()
                .AddScoped<IPromoCodeRepository, PromoCodeRepository>();
            return services;
        }

       
    }
}
