﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System;
using System.Diagnostics;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DatabaseContext: DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) 
        {
        }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            

            //modelBuilder.Entity<Employee>().Property(x => x.Role).ValueGeneratedNever();
            //modelBuilder.Entity<Employee>()
            //    .HasKey()
            //modelBuilder.Entity<CustomerPreference>()
            //    .HasOne(sc => sc.Customer)
            //    .WithMany(s => s.CustomerPreferences)
            //    .HasForeignKey(sc => sc.CustomerId);


            //modelBuilder.Entity<CustomerPreference>()
            //    .HasOne(sc => sc.Preference)
            //    .WithMany(s => s.CustomerPreferences)
            //    .HasForeignKey(sc => sc.PreferenceId);

            // modelBuilder.Entity<Cu>()
            //.HasOne<Grade>(s => s.Grade)
            //.WithMany(g => g.Students)
            //.HasForeignKey(s => s.CurrentGradeId);

        }

       protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);   
        }
    }
}
