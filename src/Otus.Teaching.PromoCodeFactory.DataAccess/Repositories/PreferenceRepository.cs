﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PreferenceRepository:EfRepository<Preference>, IPreferenceRepository
    {
        public PreferenceRepository(DatabaseContext databaseContext):base(databaseContext) 
        {
        }

        public Task<Preference> GetPreferenceByName(string name)
        {
            Preference preference = _entitySet.Where(x => x.Name == name).FirstOrDefault();

            if (preference == null) throw new ArgumentException();

            _context.Entry(preference).Collection(p => p.Customers).Load();
            return Task.FromResult(preference);
        }
    }
}
