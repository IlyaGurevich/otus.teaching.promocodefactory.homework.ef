﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository: EfRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DatabaseContext databaseContext):base(databaseContext) 
        {
        }

        public Task<IEnumerable<Customer>> GetCustomersByPreference(string preference)
        {

            List<Customer> customers = _entitySet.Where(x => x.Preferences.Any(y => y.Name == preference)).ToList();

            if (customers.Count == 0) throw new ArgumentException();

            foreach (Customer customer in customers) 
            {
                _context.Entry(customer).Collection(p => p.Preferences).Load();
                _context.Entry(customer).Collection(p => p.PromoCodes).Load();
            }

            return Task.FromResult((IEnumerable<Customer>)customers);
        }

        public override Task<Customer> GetByIdAsync(Guid id)
        {
            Customer customer = _entitySet.Where(x => x.Id == id).FirstOrDefault();

            if (customer == null) throw new ArgumentException();

            _context.Entry(customer).Collection(p => p.PromoCodes).Load();
            _context.Entry(customer).Collection(p => p.Preferences).Load();

            return Task.FromResult(customer);
        }

        public Task EditCustomer(Guid id, Customer customer) 
        {
            Customer editableCustomer = _entitySet.Find(id);

            if (editableCustomer == null) throw new ArgumentException();

            _context.Entry(editableCustomer).Collection(p => p.PromoCodes).Load();

            customer.PromoCodes= editableCustomer.PromoCodes;

            _entitySet.Remove(editableCustomer);

            _context.SaveChanges();

            _entitySet.Add(customer);

            _context.SaveChanges();

            return Task.CompletedTask;
        }
    }
}
