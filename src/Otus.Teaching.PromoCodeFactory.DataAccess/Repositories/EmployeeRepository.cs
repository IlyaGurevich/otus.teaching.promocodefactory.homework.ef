﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EmployeeRepository : EfRepository<Employee>, IEmployeeRepository
    {
        
        public EmployeeRepository(DatabaseContext databaseContext):base(databaseContext) 
        {
        }

        public Task<Employee> GetEmployeeByName(string name)
        {
            Employee employee = _entitySet.Where(x => x.FirstName + " " + x.LastName == name).FirstOrDefault();

            if(employee == null) throw new ArgumentException();

            return Task.FromResult(employee);
        }

        public override  Task<Employee> GetByIdAsync(Guid id)
        {
            Employee employee = _entitySet.Where(x => x.Id == id).FirstOrDefault();

            if (employee == null) throw new ArgumentException();

            _context.Entry(employee).Reference(x => x.Role).Load();

            return Task.FromResult(employee);
        }
    }
}
