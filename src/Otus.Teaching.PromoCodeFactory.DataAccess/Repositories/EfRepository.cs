﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly DbContext _context;
        protected readonly DbSet<T> _entitySet;

        public EfRepository(DatabaseContext databaseContext)
        {
            _entitySet = databaseContext.Set<T>();
            _context = databaseContext;
        }

        public Task AddAsync(T entity)
        {
            if (entity == null) throw new ArgumentNullException();
            _entitySet.AddAsync(entity);
            _context.SaveChanges();
            return Task.CompletedTask;
        }

        public Task DeleteByIdAsync(Guid id)
        {
            var removedEntity = _entitySet.Where(x => x.Id == id).FirstOrDefault();

            if (removedEntity == null) throw new ArgumentException();

            _entitySet.Remove(removedEntity);
            _context.SaveChanges();
            return Task.CompletedTask;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = _entitySet.ToList();
            if (entities.Count == 0) throw new ArgumentException();
            return Task.FromResult((IEnumerable<T>)entities);
        }

        public Task<IEnumerable<T>> GetByIdsAsync(List<Guid> Ids)
        {
            var entities = (IEnumerable<T>)_entitySet.Where(x => Ids.Contains(x.Id));
            if (entities == null) throw new ArgumentException();
            return Task.FromResult(entities);

        }

        public virtual Task<T> GetByIdAsync(Guid id)
        {
            var gotEntity = _entitySet.Where(x => x.Id == id).FirstOrDefault();
            if (gotEntity == null) throw new ArgumentException();
            return Task.FromResult(gotEntity);
        }

        public Task UpdateByIdAsync(T entity)
        {
            var changedEntity = _entitySet.Where(x => x.Id == entity.Id).FirstOrDefault();
            if (changedEntity == null) throw new ArgumentException();
            changedEntity = entity;
            _context.SaveChanges();
            return Task.CompletedTask;
        }
    }
}
